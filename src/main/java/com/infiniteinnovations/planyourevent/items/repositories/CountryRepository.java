package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.Country;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;


/**
 * Created by anant on 8/8/17.
 */
@Transactional
public interface CountryRepository extends CrudRepository<Country, Long> {

    @Query("SELECT country.id FROM Country country WHERE country.name=:name")
    public Long findIdByName(String name);

}
