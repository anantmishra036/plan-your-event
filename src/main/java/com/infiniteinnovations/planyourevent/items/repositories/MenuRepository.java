package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.Dish;
import com.infiniteinnovations.planyourevent.items.entities.Menu;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 8/8/17.
 */
public interface MenuRepository extends CrudRepository<Menu, Long>{
}
