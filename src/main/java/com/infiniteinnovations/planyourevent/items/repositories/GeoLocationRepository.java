package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.City;
import com.infiniteinnovations.planyourevent.items.entities.GeoLocation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 8/8/17.
 */
public interface GeoLocationRepository extends CrudRepository<GeoLocation,Long> {

    @Query("SELECT state.id FROM State state WHERE state.name=:name")
    public Long findIdByName(String name);
}
