package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.Address;
import com.infiniteinnovations.planyourevent.items.entities.Country;
import com.infiniteinnovations.planyourevent.items.entities.State;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;


/**
 * Created by anant on 8/8/17.
 */
@Transactional
public interface AddressRepository extends CrudRepository<Address, Long>, JpaSpecificationExecutor {

}
