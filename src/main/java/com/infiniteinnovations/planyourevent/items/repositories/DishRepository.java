package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.Dish;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 8/8/17.
 */
public interface DishRepository extends CrudRepository<Dish,Long>{
}
