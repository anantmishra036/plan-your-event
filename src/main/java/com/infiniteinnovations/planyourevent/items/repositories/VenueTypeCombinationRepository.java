package com.infiniteinnovations.planyourevent.items.repositories;

import com.infiniteinnovations.planyourevent.items.entities.VenueTypeCombination;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 8/8/17.
 */
public interface VenueTypeCombinationRepository extends CrudRepository<VenueTypeCombination, Long> {
}
