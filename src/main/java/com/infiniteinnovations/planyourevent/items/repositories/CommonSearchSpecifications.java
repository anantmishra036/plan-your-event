package com.infiniteinnovations.planyourevent.items.repositories;

import org.springframework.data.jpa.domain.Specification;

/**
 * Created by anant on 12/8/17.
 */
public class CommonSearchSpecifications {

    public static Specification equal(String attribute, Object value) {
        return (root, query, cb) -> cb.equal(root.get(attribute), value);
    }

    public static Specification lessThanOrEqualTo(String attribute, Integer value) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get(attribute), value);
    }

    public static Specification greaterThanOrEqualTo(String attribute, Integer value) {
        return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get(attribute), value);
    }
    public static Specification like(String attribute, String value) {
        return (root, query, cb) -> cb.like(root.get(attribute), "%" + value + "%");
    }

    public static Specification isMember(String attribute, Object value) {
        return (root, query, cb) -> cb.isMember(value,root.get(attribute));
    }

    public static Specification memberOf(String attribute, Object value) {
        return (root, query, cb) -> cb.isTrue(root.get(attribute).in(value));
    }
}
