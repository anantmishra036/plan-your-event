package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

/**
 * Created by anant on 16/6/17.
 */
@Entity
@Table(name = "venues")
@Inheritance(strategy = InheritanceType.JOINED)
public class Venue extends ServiceProvider{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "venue_id", nullable = false)
    @JsonProperty
    private List<VenueTypeCombination> venueTypeCombination;

    @ManyToMany
    @JoinTable(name="venue_event_mappings", joinColumns={@JoinColumn(name="venue_id")}
            , inverseJoinColumns={@JoinColumn(name="event_id")})
    @JsonProperty
    private List<Event> events;

    @OneToMany(cascade= CascadeType.ALL)
    @JoinTable(name = "venue_image_mappings", joinColumns = {@JoinColumn(name = "venue_id")},
            inverseJoinColumns = {@JoinColumn(name = "image_id")})
    @JsonProperty
    private List<Image> images;

    public Venue() {
    }

    public Venue(@JsonProperty String name, @JsonProperty String description, @JsonProperty Address address,
                 @JsonProperty List<Image> images, @JsonProperty List<Event> events,
                 @JsonProperty int minimumGuestsServe, @JsonProperty int maximumGuestsCapacity,
                 @JsonProperty List<VenueTypeCombination> venueTypeCombination) {
        super(name, description, address, minimumGuestsServe, maximumGuestsCapacity);
        this.venueTypeCombination = venueTypeCombination;
        this.events = events;
        this.images = images;
    }

    public List<VenueTypeCombination> getVenueTypeCombination() {
        return venueTypeCombination;
    }

    public void setVenueTypeCombination(List<VenueTypeCombination> venueTypeCombination) {
        this.venueTypeCombination = venueTypeCombination;
    }
}
