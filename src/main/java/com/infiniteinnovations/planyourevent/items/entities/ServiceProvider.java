package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by anant on 15/6/17.
 */
@MappedSuperclass
public abstract class ServiceProvider extends BaseEntity{

    @Column(name = "name", nullable = false)
    @JsonProperty
    private String name;

    @Column(name = "description", length = 2048)
    @JsonProperty
    private String Description;

    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty
    private Address address;

    @Column(name = "minimum_guests_serve", nullable = false)
    @JsonProperty
    private int minimumGuestsServe;

    @Column(name = "maximum_guests_capacity", nullable = false)
    @JsonProperty
    private int maximumGuestsCapacity;

    public ServiceProvider() {
    }

    public ServiceProvider(@JsonProperty String name, @JsonProperty String description, @JsonProperty Address address,
                @JsonProperty int minimumGuestsServe, @JsonProperty int maximumGuestsCapacity) {
        this.name = name;
        Description = description;
        this.address = address;
        this.minimumGuestsServe = minimumGuestsServe;
        this.maximumGuestsCapacity = maximumGuestsCapacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getMinimumGuestsServe() {
        return minimumGuestsServe;
    }

    public void setMinimumGuestsServe(int minimumGuestsServe) {
        this.minimumGuestsServe = minimumGuestsServe;
    }

    public int getMaximumGuestsCapacity() {
        return maximumGuestsCapacity;
    }

    public void setMaximumGuestsCapacity(int maximumGuestsCapacity) {
        this.maximumGuestsCapacity = maximumGuestsCapacity;
    }

}
