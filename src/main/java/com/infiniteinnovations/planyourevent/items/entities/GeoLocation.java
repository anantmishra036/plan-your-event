package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;

/**
 * Created by anant on 15/6/17.
 */
@Entity
@Table(name = "geo_locations")
public class GeoLocation extends BaseEntity{

    @Column(name = "latitude", nullable = false)
    @JsonProperty
    private double latitude;

    @Column(name = "longitude", nullable = false)
    @JsonProperty
    private double longitude;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    public GeoLocation() {
    }

    public GeoLocation(@JsonProperty double latitude, @JsonProperty double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
