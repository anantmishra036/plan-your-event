package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by anant on 15/6/17.
 */
@Entity
@Table(name="images")
public class Image extends BaseEntity{

    @Column(name = "url", nullable = false)
    @JsonProperty
    private String url;

    public Image() {
    }

    public Image(@JsonProperty String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
