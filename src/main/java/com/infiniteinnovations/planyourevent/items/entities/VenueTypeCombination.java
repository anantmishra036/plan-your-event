package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by anant on 17/6/17.
 */
@Entity
@Table(name = "venue_type_combinations")
public class VenueTypeCombination extends BaseEntity{

    @ManyToMany
    @JoinTable(name="venue_venue_type_combination_mapping", joinColumns={@JoinColumn(name="venue_type_combination_id", referencedColumnName="id")}
            , inverseJoinColumns={@JoinColumn(name="venue_id", referencedColumnName="id")})
    @JsonProperty
    private List<VenueType> venueTypes;

    @Column(name = "description", length = 1024)
    @JsonProperty
    private String description;

    @Column(name = "no_of_guests")
    @JsonProperty
    private int noOfGuests;

    @JsonProperty
    private float price;

    public VenueTypeCombination() {
    }

    public VenueTypeCombination(@JsonProperty List<VenueType> venueTypes, @JsonProperty String description,
                                @JsonProperty int noOfGuests, @JsonProperty float price) {
        this.venueTypes = venueTypes;
        this.description = description;
        this.noOfGuests = noOfGuests;
        this.price = price;
    }

    public List<VenueType> getVenueTypes() {
        return venueTypes;
    }

    public void setVenueTypes(List<VenueType> venueTypes) {
        this.venueTypes = venueTypes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNoOfGuests() {
        return noOfGuests;
    }

    public void setNoOfGuests(int noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
