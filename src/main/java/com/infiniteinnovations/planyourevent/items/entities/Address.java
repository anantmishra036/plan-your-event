package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;

/**
 * Created by anant on 15/6/17.
 */
@Entity
@Table(name = "addresses")
public class Address extends BaseEntity{

    @Column(name="first_line", nullable = false)
    @JsonProperty
    private String firstLine;

    @Column(name="second_line")
    @JsonProperty
    private String secondLine;

    @Column(name="landmark")
    @JsonProperty
    private String landmark;

    @Column(name="locality")
    @JsonProperty
    private String locality;

    @ManyToOne
    @JoinColumn(name="city_id", nullable = false)
    @JsonProperty
    private City city;

    @Column(name="zip_code", nullable = false)
    @JsonProperty
    private Integer zipCode;

    @OneToOne(mappedBy = "address")
    @JsonProperty
    private GeoLocation geoLocation;

    protected Address() {}

    public Address(@JsonProperty String firstLine, @JsonProperty String secondLine, @JsonProperty String landmark,
                   @JsonProperty String locality, @JsonProperty City city, @JsonProperty Integer zipCode, @JsonProperty GeoLocation geoLocation) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.landmark = landmark;
        this.locality = locality;
        this.city = city;
        this.zipCode = zipCode;
        this.geoLocation = geoLocation;
    }

    public String getFirstLine() {
        return firstLine;
    }

    public void setFirstLine(String firstLine) {
        this.firstLine = firstLine;
    }

    public String getSecondLine() {
        return secondLine;
    }

    public void setSecondLine(String secondLine) {
        this.secondLine = secondLine;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }
}
