package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;

/**
 * Created by anant on 6/8/17.
 */
@Entity
@Table(name = "cities", uniqueConstraints = {@UniqueConstraint(columnNames = {"name","state_id"})})
public class City extends BaseEntity {

    @Column(unique = true, nullable = false)
    @JsonProperty
    private String name;

    @ManyToOne
    @JoinColumn(name = "state_id", nullable = false)
    @JsonProperty
    private State state;

    public City() {
    }

    public City(@JsonProperty String name, @JsonProperty State state) {
        this.name = name;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (!name.equals(city.name)) return false;
        return state.equals(city.state);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + state.hashCode();
        return result;
    }
}
