package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;

/**
 * Created by anant on 6/8/17.
 */
@Entity
@Table(name = "states", uniqueConstraints = {@UniqueConstraint(columnNames = {"name","country_id"})})
public class State extends BaseEntity {

    @Column(unique = true, nullable = false)
    @JsonProperty
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    @JsonProperty
    private Country country;

    protected State() {

    }

    public State(@JsonProperty String name, @JsonProperty Country country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        if (!name.equals(state.name)) return false;
        return country.equals(state.country);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + country.hashCode();
        return result;
    }
}
