package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by anant on 17/6/17.
 */
@Entity
@Table(name = "menus")
public class Menu extends BaseEntity{

    @Column(name = "is_vegeterian", nullable = false)
    @JsonProperty
    private boolean isVegeterian;

    @ManyToMany
    @JoinTable(name = "menu_dish_type_mappings", joinColumns = {@JoinColumn(name = "menu_id")},
            inverseJoinColumns = {@JoinColumn(name = "dish_type_id")})
    @JsonProperty
    private List<DishType> dishType;

    @ManyToMany
    @JoinTable(name = "menu_dish_mappings", joinColumns = {@JoinColumn(name = "menu_id")},
            inverseJoinColumns = {@JoinColumn(name = "dish_id")})
    @JsonProperty
    private List<Dish> dishes;

    @Column(name = "description", length = 1024)
    @JsonProperty
    private String description;

    @Column(name = "price")
    @JsonProperty
    private float price;

    public Menu() {
    }

    public Menu(@JsonProperty boolean isVegeterian, @JsonProperty List<DishType> dishType,
                @JsonProperty List<Dish> dishes, @JsonProperty String description, @JsonProperty float price) {
        this.isVegeterian = isVegeterian;
        this.dishType = dishType;
        this.dishes = dishes;
        this.description = description;
        this.price = price;
    }

    public boolean isVegeterian() {
        return isVegeterian;
    }

    public void setVegeterian(boolean vegeterian) {
        isVegeterian = vegeterian;
    }

    public List<DishType> getDishType() {
        return dishType;
    }

    public void setDishType(List<DishType> dishType) {
        this.dishType = dishType;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
