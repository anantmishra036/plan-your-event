package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by anant on 16/6/17.
 */
@Entity
@Table(name = "venue_types")
public class VenueType extends BaseEntity {

    @Column(name = "name", nullable = false)
    @JsonProperty
    private String name;

    public VenueType() {
    }

    public VenueType(@JsonProperty String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
