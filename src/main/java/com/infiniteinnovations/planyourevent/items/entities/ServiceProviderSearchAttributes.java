package com.infiniteinnovations.planyourevent.items.entities;

/**
 * Created by anant on 12/8/17.
 */
public class ServiceProviderSearchAttributes {

    private Long cityId;
    private Long eventId;
    private Integer minimumGuestsServe;
    private Integer maximumGuestsCapacity;
    private String name;

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Integer getMinimumGuestsServe() {
        return minimumGuestsServe;
    }

    public void setMinimumGuestsServe(Integer minimumGuestsServe) {
        this.minimumGuestsServe = minimumGuestsServe;
    }

    public Integer getMaximumGuestsCapacity() {
        return maximumGuestsCapacity;
    }

    public void setMaximumGuestsCapacity(Integer maximumGuestsCapacity) {
        this.maximumGuestsCapacity = maximumGuestsCapacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
