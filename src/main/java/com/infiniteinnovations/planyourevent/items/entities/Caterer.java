package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

/**
 * Created by anant on 17/6/17.
 */
@Entity
@Table(name = "caterers")
@Inheritance(strategy = InheritanceType.JOINED)
public class Caterer extends ServiceProvider{

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "caterer_id", nullable = false)
    @JsonProperty
    private List<Menu> menus;

    @ManyToMany
    @JoinTable(name="caterer_event_mappings", joinColumns={@JoinColumn(name="caterer_id")}
            , inverseJoinColumns={@JoinColumn(name="event_id")})
    @JsonProperty
    private List<Event> events;

    @OneToMany(cascade= CascadeType.ALL)
    @JoinTable(name = "caterer_image_mappings", joinColumns = {@JoinColumn(name = "caterer_id")},
            inverseJoinColumns = {@JoinColumn(name = "image_id")})
    @JsonProperty
    private List<Image> images;

    public Caterer() {
    }

    public Caterer(@JsonProperty String name, @JsonProperty String description, @JsonProperty Address address,
                 @JsonProperty List<Image> images, @JsonProperty List<Event> events,
                 @JsonProperty int minimumGuestsServe, @JsonProperty int maximumGuestsCapacity,
                 @JsonProperty List<Menu> menus) {
        super(name, description, address, minimumGuestsServe, maximumGuestsCapacity);
        this.menus = menus;
        this.events = events;
        this.images = images;
    }
}
