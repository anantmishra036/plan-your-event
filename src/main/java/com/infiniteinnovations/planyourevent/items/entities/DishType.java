package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by anant on 18/6/17.
 */
@Entity
@Table(name = "dish_types")
public class DishType extends BaseEntity{

    @Column(name = "name", nullable = false, unique = true)
    @JsonProperty
    private String name;

    @Column(name = "is_common_dish_type", nullable = false)
    @JsonProperty
    private boolean isCommonDishType;

    public DishType() {
    }

    public DishType(@JsonProperty String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
