package com.infiniteinnovations.planyourevent.items.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by anant on 16/6/17.
 */
@Entity
@Table(name = "events")
public class Event extends BaseEntity {

    @Column(name = "name", nullable = false)
    @JsonProperty
    private String name;

    public Event() {
    }

    public Event(@JsonProperty String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return name.equals(event.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
