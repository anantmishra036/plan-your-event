package com.infiniteinnovations.planyourevent.items.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infiniteinnovations.planyourevent.items.dto.AggregateResponseDto;
import com.infiniteinnovations.planyourevent.items.entities.Caterer;
import com.infiniteinnovations.planyourevent.items.entities.ServiceProviderSearchAttributes;
import com.infiniteinnovations.planyourevent.items.entities.Venue;
import com.infiniteinnovations.planyourevent.items.service.CatererService;
import com.infiniteinnovations.planyourevent.items.service.VenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by anant on 11/8/17.
 */

@RestController
@RequestMapping(path = VenueController.CATERER_PATH)
public class VenueController {

    public static final String CATERER_PATH = "/venues";
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    VenueService venueService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Venue createCaterer(@RequestBody String venueJson) throws IOException {
        Venue venue = objectMapper.readValue(venueJson, Venue.class);
       return venueService.saveVenue(venue);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AggregateResponseDto getCaterersBasedOnFilters(@RequestParam(value = "cityId") Long cityId,
                                                          @RequestParam(value = "eventId", required = false) Long eventId,
                                                          @RequestParam(value = "maximumGuestsCapacity", required = false) Integer maximumGuestsCapacity,
                                                          @RequestParam(value = "minimumGuestsServe", required = false) Integer minimumGuestsServe,
                                                          @RequestParam(value = "name", required = false) String name) {
        ServiceProviderSearchAttributes searchAttributes = new ServiceProviderSearchAttributes();
        searchAttributes.setCityId(cityId);
        searchAttributes.setEventId(eventId);
        searchAttributes.setMaximumGuestsCapacity(maximumGuestsCapacity);
        searchAttributes.setMinimumGuestsServe(minimumGuestsServe);
        searchAttributes.setName(name);
        return new AggregateResponseDto().put("venueSearchResponse", venueService.findVenuesBasedOnFilters(searchAttributes));


    }
    @RequestMapping(path = "{venue_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Venue getCaterer(@PathVariable("venue_id") Long id) {;
        return venueService.findVenue(id);
    }
}
