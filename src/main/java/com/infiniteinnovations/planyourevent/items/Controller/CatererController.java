package com.infiniteinnovations.planyourevent.items.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infiniteinnovations.planyourevent.items.dto.AggregateResponseDto;
import com.infiniteinnovations.planyourevent.items.entities.Caterer;
import com.infiniteinnovations.planyourevent.items.entities.ServiceProviderSearchAttributes;
import com.infiniteinnovations.planyourevent.items.service.CatererService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by anant on 11/8/17.
 */

@RestController
@RequestMapping(path = CatererController.CATERER_PATH)
public class CatererController {

    public static final String CATERER_PATH = "/caterers";
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    CatererService catererService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Caterer createCaterer(@RequestBody String catererJson) throws IOException {
        Caterer caterer = objectMapper.readValue(catererJson, Caterer.class);
       return catererService.saveCaterer(caterer);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AggregateResponseDto getCaterersBasedOnFilters(@RequestParam(value = "cityId") Long cityId,
                                                          @RequestParam(value = "eventId", required = false) Long eventId,
                                                          @RequestParam(value = "maximumGuestsCapacity", required = false) Integer maximumGuestsCapacity,
                                                          @RequestParam(value = "minimumGuestsServe", required = false) Integer minimumGuestsServe,
                                                          @RequestParam(value = "name", required = false) String name) {
        ServiceProviderSearchAttributes searchAttributes = new ServiceProviderSearchAttributes();
        searchAttributes.setCityId(cityId);
        searchAttributes.setEventId(eventId);
        searchAttributes.setMaximumGuestsCapacity(maximumGuestsCapacity);
        searchAttributes.setMinimumGuestsServe(minimumGuestsServe);
        searchAttributes.setName(name);
        return new AggregateResponseDto().put("catererSearchResponse", catererService.findCaterersBasedOnFilters(searchAttributes));


    }

    @RequestMapping(path = "{catererid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Caterer getCaterer(@PathVariable("catererid") Long id) {;
        return catererService.findCaterer(id);
    }

}
