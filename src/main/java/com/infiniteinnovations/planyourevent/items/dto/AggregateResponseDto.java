package com.infiniteinnovations.planyourevent.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by anant on 12/8/17.
 */
public class AggregateResponseDto implements Serializable{

    @JsonProperty
    private final Map<String, Object> responseObject;

    public AggregateResponseDto() {
        responseObject = new TreeMap<>();
    }

    public AggregateResponseDto put(String key, Object value) {
        responseObject.put(key, value);
        return this;
    }
}
