package com.infiniteinnovations.planyourevent.items.service;

import com.infiniteinnovations.planyourevent.items.entities.*;
import com.infiniteinnovations.planyourevent.items.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.infiniteinnovations.planyourevent.items.repositories.CommonSearchSpecifications.*;

/**
 * Created by anant on 11/8/17.
 */
@Service
public class CatererService {

    @Autowired
    CatererRepository catererRepository;

    @Autowired
    AddressService addressService;

    @Autowired
    EventRepository eventRepository;


    @Transactional
    public Caterer saveCaterer(Caterer caterer) {
        return catererRepository.save(caterer);
    }

    public Caterer findCaterer(Long id) {
        return catererRepository.findOne(id);
    }

    public List<Caterer> findCaterersBasedOnFilters(ServiceProviderSearchAttributes searchAttributes) {

        List<Address> addresses = addressService.findCaterersBasedOnFilters(searchAttributes.getCityId());
        Specification<Caterer> specification = Specifications.where(memberOf("address", addresses));
        if(searchAttributes.getMinimumGuestsServe() != null) {
            specification = Specifications.where(specification)
                    .and(lessThanOrEqualTo("minimumGuestsServe",searchAttributes.getMinimumGuestsServe()));
        }

        if(searchAttributes.getMaximumGuestsCapacity() != null) {
            specification = Specifications.where(specification)
                    .and(greaterThanOrEqualTo("maximumGuestsCapacity",searchAttributes.getMaximumGuestsCapacity()));
        }

        if(searchAttributes.getEventId() != null) {
            Event event = eventRepository.findOne(searchAttributes.getEventId());
            specification = Specifications.where(specification)
                    .and(isMember("events",event));
        }

        if(searchAttributes.getName() != null) {
            specification = Specifications.where(specification)
                    .and(like("name",searchAttributes.getName()));
        }

        return catererRepository.findAll(specification);
    }
}
