package com.infiniteinnovations.planyourevent.items.service;

import com.infiniteinnovations.planyourevent.items.entities.*;
import com.infiniteinnovations.planyourevent.items.repositories.AddressRepository;
import com.infiniteinnovations.planyourevent.items.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.infiniteinnovations.planyourevent.items.repositories.CommonSearchSpecifications.*;
import static com.infiniteinnovations.planyourevent.items.repositories.CommonSearchSpecifications.like;

/**
 * Created by anant on 12/8/17.
 */
@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    CityRepository cityRepository;

    public List<Address> findCaterersBasedOnFilters(Long cityId) {

        City city = cityRepository.findOne(cityId);
        Specification<Address> specification = Specifications.where(equal("city",city));

        return addressRepository.findAll(specification);
    }
}
