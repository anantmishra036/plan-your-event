package com.infiniteinnovations.planyourevent.helpandsupport.repositories;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.FrequentlyAskedQuestionAnswer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 24/8/17.
 */
public interface FrequentlyAskedQuestionAnswerRepository extends CrudRepository<FrequentlyAskedQuestionAnswer, Long> {
}
