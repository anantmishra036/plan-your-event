package com.infiniteinnovations.planyourevent.helpandsupport.repositories;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.ManagementInformation;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by anant on 24/8/17.
 */
public interface ManagementInformationRepository extends CrudRepository<ManagementInformation, Long> {
}
