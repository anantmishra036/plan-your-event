package com.infiniteinnovations.planyourevent.helpandsupport.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.*;

/**
 * Created by anant on 24/8/17.
 */
@Entity
@Table(name = "feedbacks")
public class Feedback extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "feedback_type_id", nullable = false)
    @JsonProperty
    private FeedbackType feedbackType;

    @Column(name = "description", nullable = false, length = 2048)
    @JsonProperty
    private String description;

    public Feedback() {
    }

    public Feedback(FeedbackType feedbackType, String description) {
        this.feedbackType = feedbackType;
        this.description = description;
    }

    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
