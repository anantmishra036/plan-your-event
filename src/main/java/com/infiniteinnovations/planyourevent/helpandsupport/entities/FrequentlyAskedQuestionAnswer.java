package com.infiniteinnovations.planyourevent.helpandsupport.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by anant on 24/8/17.
 */

@Entity
@Table(name = "frequently_asked_question_answers")
public class FrequentlyAskedQuestionAnswer extends BaseEntity{

    @Column(name = "question", nullable = false, length = 512)
    @JsonProperty
    private String question;

    @Column(name = "answer", nullable = false, length = 2048)
    @JsonProperty
    private String answer;

    public FrequentlyAskedQuestionAnswer() {
    }

    public FrequentlyAskedQuestionAnswer(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
