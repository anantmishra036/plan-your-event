package com.infiniteinnovations.planyourevent.helpandsupport.entities;

import com.infiniteinnovations.planyourevent.common.BaseEntity;
import com.infiniteinnovations.planyourevent.items.entities.Image;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by anant on 24/8/17.
 */
@Entity
@Table(name = "management_informations")
public class ManagementInformation extends BaseEntity{

    private String name;

    private String designation;

    private String description;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image image;

    public ManagementInformation() {
    }

    public ManagementInformation(String name, String designation, String description, Image image) {
        this.name = name;
        this.designation = designation;
        this.description = description;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
