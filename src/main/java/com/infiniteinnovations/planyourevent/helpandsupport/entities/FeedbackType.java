package com.infiniteinnovations.planyourevent.helpandsupport.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by anant on 24/8/17.
 */
@Entity
@Table(name = "feedback_types")
public class FeedbackType extends BaseEntity {

    @JsonProperty
    private String name;

    public FeedbackType() {
    }

    public FeedbackType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
