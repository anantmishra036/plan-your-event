package com.infiniteinnovations.planyourevent.helpandsupport.controllers;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.ManagementInformation;
import com.infiniteinnovations.planyourevent.helpandsupport.services.FrequentlyAskedQuestionAnswerService;
import com.infiniteinnovations.planyourevent.helpandsupport.services.ManagementInformationService;
import com.infiniteinnovations.planyourevent.items.dto.AggregateResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anant on 24/8/17.
 */
@RestController
@RequestMapping(path = FeedbackController.CONTACT_US_PATH+AboutUsController.ABOUT_US_PATH)
public class AboutUsController {
    public static final String ABOUT_US_PATH = "/aboutus";

    @Autowired
    ManagementInformationService managementInformationService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AggregateResponseDto getAboutUsInformation() {
        AggregateResponseDto aggregateResponseDto = new AggregateResponseDto();
        aggregateResponseDto.put("allManagementInformation", managementInformationService.getAllManagementInformation());
        aggregateResponseDto.put("siteInformation", "testing site response");

        return aggregateResponseDto;
    }
}
