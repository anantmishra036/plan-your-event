package com.infiniteinnovations.planyourevent.helpandsupport.controllers;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.Feedback;
import com.infiniteinnovations.planyourevent.helpandsupport.services.FrequentlyAskedQuestionAnswerService;
import com.infiniteinnovations.planyourevent.items.dto.AggregateResponseDto;
import com.infiniteinnovations.planyourevent.items.entities.ServiceProviderSearchAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anant on 24/8/17.
 */
@RestController
@RequestMapping(path = FeedbackController.CONTACT_US_PATH+FrequentlyAskedQuestionAnswerController.FAQ_PATH)
public class FrequentlyAskedQuestionAnswerController {

    public static final String FAQ_PATH = "/faq";

    @Autowired
    FrequentlyAskedQuestionAnswerService frequentlyAskedQuestionAnswerService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AggregateResponseDto getFAQs() {
        return new AggregateResponseDto().put("faqResponse", frequentlyAskedQuestionAnswerService.getAllFAQs());


    }
}
