package com.infiniteinnovations.planyourevent.helpandsupport.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infiniteinnovations.planyourevent.helpandsupport.entities.Feedback;
import com.infiniteinnovations.planyourevent.helpandsupport.services.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by anant on 24/8/17.
 */
@RestController
@RequestMapping(path = FeedbackController.CONTACT_US_PATH+FeedbackController.FEEDBACK_PATH)
public class FeedbackController {

    public static final String FEEDBACK_PATH = "/feedback";
    public static final String CONTACT_US_PATH = "/contactus";

    private static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    FeedbackService feedbackService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Feedback createFeedback(@RequestBody String feedbackJson) throws IOException {
        Feedback feedback = objectMapper.readValue(feedbackJson, Feedback.class);
        return feedbackService.saveFeedback(feedback);
    }
}
