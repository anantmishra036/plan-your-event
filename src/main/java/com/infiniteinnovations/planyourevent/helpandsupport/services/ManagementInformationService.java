package com.infiniteinnovations.planyourevent.helpandsupport.services;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.ManagementInformation;
import com.infiniteinnovations.planyourevent.helpandsupport.repositories.ManagementInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anant on 24/8/17.
 */
@Service
public class ManagementInformationService {

    @Autowired
    ManagementInformationRepository managementInformationRepository;

    public List<ManagementInformation> getAllManagementInformation(){
        return (List<ManagementInformation>) managementInformationRepository.findAll();
    }
}
