package com.infiniteinnovations.planyourevent.helpandsupport.services;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.Feedback;
import com.infiniteinnovations.planyourevent.helpandsupport.repositories.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by anant on 24/8/17.
 */
@Service
public class FeedbackService {

    @Autowired
    FeedbackRepository feedbackRepository;


    @Transactional
    public Feedback saveFeedback(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }
}
