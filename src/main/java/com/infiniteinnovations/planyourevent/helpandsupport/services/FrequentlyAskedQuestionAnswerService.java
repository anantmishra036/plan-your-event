package com.infiniteinnovations.planyourevent.helpandsupport.services;

import com.infiniteinnovations.planyourevent.helpandsupport.entities.FrequentlyAskedQuestionAnswer;
import com.infiniteinnovations.planyourevent.helpandsupport.repositories.FrequentlyAskedQuestionAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by anant on 24/8/17.
 */
@Service
public class FrequentlyAskedQuestionAnswerService {
    @Autowired
    FrequentlyAskedQuestionAnswerRepository frequentlyAskedQuestionAnswerRepository;

    public List<FrequentlyAskedQuestionAnswer> getAllFAQs() {
        return (List<FrequentlyAskedQuestionAnswer>)frequentlyAskedQuestionAnswerRepository.findAll();
    }
}
