package com.infiniteinnovations.planyourevent.security.configurations;

import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.accounts.services.SessionServiceImpl;
import com.infiniteinnovations.planyourevent.accounts.services.UserDetailsServiceImpl;
import com.infiniteinnovations.planyourevent.security.handlers.RequestInterceptorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    SessionServiceImpl sessionService;


    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       // registry.addInterceptor(new ThemeInterceptor()).addPathPatterns("/**").excludePathPatterns("/admin/**");
       // registry.addInterceptor(new RequestProcessingTimeInterceptor()).addPathPatterns("**/venues/*");
        registry.addInterceptor(new RequestInterceptorHandler(sessionService, userDetailsService)).addPathPatterns("/venues/*");
       //`registry.addInterceptor(new RequestProcessingTimeInterceptor()).addPathPatterns("/venues/**");
       // registry.addInterceptor(new RequestProcessingTimeInterceptor()).addPathPatterns("/**");
    }

}