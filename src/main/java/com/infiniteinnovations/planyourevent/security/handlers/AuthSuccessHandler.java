package com.infiniteinnovations.planyourevent.security.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.accounts.repositories.UserDetailsImpl;
import com.infiniteinnovations.planyourevent.accounts.services.SessionServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

@Component
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthSuccessHandler.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private SessionServiceImpl session;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        User user = (User) authentication.getPrincipal();

        LOGGER.info(user.getUsername() + " got is connected ");

      //  UUID uid = UUID.fromString(user.getUsername()+ user.getPassword());
        String authToken = UUID.randomUUID().toString();
        //  Cookie cookie = new Cookie("auth_token", user.getUsername()+"-----"+user.getPassword());
        Cookie cookie = new Cookie("auth_token",authToken);
        //cookie.setHttpOnly(true);
        response.addCookie(cookie);
        //response.setHeader("auth_token", user.getUsername()+"-----"+user.getPassword());
        response.setHeader("auth_token",authToken);
        session.saveSessionInfo(authToken, null,user.getUsername ().toString());
    }
}
