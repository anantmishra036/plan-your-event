package com.infiniteinnovations.planyourevent.security.handlers;

import com.infiniteinnovations.planyourevent.accounts.entities.Session;
import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.accounts.services.SessionServiceImpl;
import com.infiniteinnovations.planyourevent.accounts.services.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RequestInterceptorHandler extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory
            .getLogger(RequestInterceptorHandler.class);

	private SessionServiceImpl sessionService;

	private UserDetailsServiceImpl userDetailsService;

	public	RequestInterceptorHandler(SessionServiceImpl sessionService,UserDetailsServiceImpl userDetailsService){
		this.sessionService = sessionService;
		this.userDetailsService = userDetailsService;
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		long startTime = System.currentTimeMillis();
		logger.info("Request URL::" + request.getRequestURL().toString()
				+ ":: Start Time=" + System.currentTimeMillis());
		request.setAttribute("startTime", startTime);

		boolean progress = false;
		logger.info(" header : " + request.getHeader("auth_token"));
		UserDetails userDetails = checkAuthTokenAndReturnUser(request.getHeader("auth_token"));

		//if returned false, we need to make sure 'response' is sent
		if(userDetails != null) {
			logger.info("User : " + userDetails.getUsername() );
			progress = true;
		}
		else{
			//	response.sendRedirect(request.getContextPath() + "/login.html");
		}
		logger.info("Progess value = " + progress);
		return progress;
	}


	/**
	 * Method to check if auth token is correct
	 * @param token
	 * @return
	 */
	public UserDetails checkAuthTokenAndReturnUser(String token){
		UserDetails userDetails = null;
		if(token != null){
			Session session = sessionService.getSessionById(token) ;
			if(session != null) {
				String userName = session.getUserId();
				userDetails = userDetailsService.loadUserByUsername(userName);
			}
		}
		//Thread.currentThread().
		return userDetails;
	}
}