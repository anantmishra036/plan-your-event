package com.infiniteinnovations.planyourevent.security.entities;

import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by anant on 16/8/17.
 */
public class LoginDetail extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name= "access_token")
    private String accessToken;

    private boolean isValid;
}
