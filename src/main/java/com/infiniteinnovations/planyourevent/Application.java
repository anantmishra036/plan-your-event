package com.infiniteinnovations.planyourevent;

import com.infiniteinnovations.planyourevent.items.entities.Caterer;
import com.infiniteinnovations.planyourevent.items.service.CatererService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}



}
