package com.infiniteinnovations.planyourevent.common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by anant on 15/6/17.
 */
public enum YesOrNo {
    @JsonProperty
    YES,
    @JsonProperty
    NO
}
