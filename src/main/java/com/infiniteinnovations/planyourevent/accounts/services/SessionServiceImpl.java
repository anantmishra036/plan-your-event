package com.infiniteinnovations.planyourevent.accounts.services;

import com.infiniteinnovations.planyourevent.accounts.entities.Session;
import com.infiniteinnovations.planyourevent.accounts.repositories.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl {

    @Autowired
    SessionRepository sessionRepository;

    public Session getSessionById(String token){
        return sessionRepository.findByAuthToken(token);
    }

    public void saveSessionInfo(String token, String expireTime, String userId){
        Session session =  new Session(token, "15", userId);
        sessionRepository.save(session);
    }
}
