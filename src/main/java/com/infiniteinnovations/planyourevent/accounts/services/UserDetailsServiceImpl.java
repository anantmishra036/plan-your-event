package com.infiniteinnovations.planyourevent.accounts.services;


import com.infiniteinnovations.planyourevent.accounts.entities.Role;
import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.accounts.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(userName);
        return user;
    }

    @Transactional
    public UserDetails save(User user) throws UsernameNotFoundException {
        return userRepository.save(user);

    }
}