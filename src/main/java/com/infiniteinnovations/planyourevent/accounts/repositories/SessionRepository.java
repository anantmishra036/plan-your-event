package com.infiniteinnovations.planyourevent.accounts.repositories;

import com.infiniteinnovations.planyourevent.accounts.entities.Role;
import com.infiniteinnovations.planyourevent.accounts.entities.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session,Long> {

    public Session findByAuthToken(String token);
}
