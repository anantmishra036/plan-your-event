package com.infiniteinnovations.planyourevent.accounts.repositories;

import com.infiniteinnovations.planyourevent.accounts.entities.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by anant on 14/8/17.
 */
@Repository
public interface RoleRepository extends CrudRepository<Role,Long> {
}
