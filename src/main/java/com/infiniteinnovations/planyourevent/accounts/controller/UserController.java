package com.infiniteinnovations.planyourevent.accounts.controller;

import com.infiniteinnovations.planyourevent.accounts.entities.User;
import com.infiniteinnovations.planyourevent.accounts.services.SecurityServiceImpl;
import com.infiniteinnovations.planyourevent.accounts.services.UserDetailsServiceImpl;
import com.infiniteinnovations.planyourevent.accounts.services.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = User.USER)
public class UserController {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = User.REGISTRATION, method = RequestMethod.POST)
    public String registration(@RequestParam("firstName") String firstName,
                               @RequestParam(value = "lastName", required = false) String lastName,
                               @RequestParam(value = "email", required = false) String email,
                               @RequestParam("mobileNumber") String mobileNumber,
                               @RequestParam("password") String password,
                               @RequestParam("passwordConfirm") String passwordConfirm) {

        User user = new User(firstName,lastName,Long.parseLong(mobileNumber),email,password);
        userValidator.validate(user, null);


        userDetailsService.save(user);

        securityService.autologin(String.valueOf(user.getUsername()), user.getPassword());

        return "success";
    }
}