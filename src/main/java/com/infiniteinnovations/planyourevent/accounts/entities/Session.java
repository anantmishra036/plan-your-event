package com.infiniteinnovations.planyourevent.accounts.entities;

import com.infiniteinnovations.planyourevent.common.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_session")
public class Session extends BaseEntity{


    @Column(name = "authToken")
    private String authToken;

    @Column(name = "expireTime")
    private String expireTime;

   // @OneToOne(mappedBy = "users")
    @Column(name="user_id")
    private String userId;

    public Session() {
    }

    public Session(String authToken, String expireTime, String userId) {
        this.authToken = authToken;
        this.expireTime = "15";
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
